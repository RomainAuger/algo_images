## Détection de pistes d'atterrissage

### Exemple 1

|           Image d'origine           |       Détection des pistes       |
| :---------------------------------: | :------------------------------: |
| ![](/Images/Exemples/aeroport1.png) | ![](/Images/Exemples/pistes.JPG) |

### Exemple 2

|           Image d'origine           |       Détection des pistes        |
| :---------------------------------: | :-------------------------------: |
| ![](/Images/Exemples/aeroport2.png) | ![](/Images/Exemples/pistes2.JPG) |


## Détection de la route via partage des eaux

|         Image d'origine         |      Détection de la route      |
| :-----------------------------: | :-----------------------------: |
| ![](/Images/Exemples/route.png) | ![](/Images/Exemples/route.JPG) |

## Calcul de la largeur de grains de riz

|        Image d'origine         |       Calcul de la largeur       |
| :----------------------------: | :-------------------------------: |
| ![](/Images/Exemples/rice.png) | ![](/Images/Exemples/largeur.JPG) |